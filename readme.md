# Lottee
---

## Backend

_pip install -r requirements.txt_

_python manage.py makemigrations_

_python manage.py migrate_

_python manage.py runserver_


## Frontend

_cd frontend_

### Local

_yarn_

_yarn dev_

---

### Deploy
_yarn_

_yarn build_

_yarn start_

